# 用来记录知识点和笔试题

**愿所有的付出都有回报，愿所有的善良都被时光温柔以待**

</br>

**每日学习，终身学习，实时记录**



**站在巨人的肩膀上**<br>



1. [java全栈体系](https://pdai.tech)

2. [JavaGuide面试突击版](https://snailclimb.gitee.io/javaguide-interview/)

   [JavaGuide](https://javaguide.cn/)

3. [编程导航-鱼皮](https://www.code-nav.cn/)
4. [力扣Top](https://codetop.cc/home)

5. [java知音](https://www.javazhiyin.com/mst)

6. [advanced-java](https://doocs.gitee.io/advanced-java/)

7. [r2coding](https://www.r2coding.com/#/)

8. [B站-尚硅谷](https://www.bilibili.com/read/cv5216534?spm_id_from=333.788.b_636f6d6d656e74.4)

9. [B站-黑马](https://www.bilibili.com/read/cv9965357?spm_id_from=333.788.b_636f6d6d656e74.6)

10. [吴师兄学编程](https://www.cxyxiaowu.com/)

    [github地址](https://github.com/MisterBooo/LeetCodeAnimation)

11. [cysnote](http://www.cyc2018.xyz/)

12. [java技术爱好者(github)](https://github.com/yehongzhi/learningSummary)

