# 常见面试题，早日成为面霸

<center><font size=4>欧科云链</font>
</center>


1. 说下ThreadLocal 的实现和使用，remove 和 set(null) 有什么区别

   https://blog.csdn.net/Dongguabai/article/details/121266686

2. 谈谈你对Java并发包的理解，你用过哪些，比如讲讲AQS，CAS

   https://blog.csdn.net/striveb/article/details/86761900

   https://www.cnblogs.com/waterystone/p/4920797.html

3. 谈谈MySQL数据库，比如索引、事务隔离级别，B+树和B树的区别

   | 事务隔离级别                 | 脏读 | 不可重复读 | 幻读 |
   | ---------------------------- | ---- | ---------- | ---- |
   | 读未提交（read-uncommitted） | 是   | 是         | 是   |
   | 不可重复读（read-committed） | 否   | 是         | 是   |
   | 可重复读（repeatable-read）  | 否   | 否         | 是   |
   | 串行化（serializable）       | 否   | 否         | 否   |

   ### B树的特点： 

   1. 节点排序 
   2. ⼀个节点了可以存多个元素，多个元素也排序了 

   ### B+树的特点： 

   1. 拥有B树的特点 

   2. 叶⼦节点之间有指针 

   3. ⾮叶⼦节点上的元素在叶⼦节点上都冗余了，也就是叶⼦节点中存储了所有的元素，并且排好顺序

   Mysql索引使⽤的是B+树，因为索引是⽤来加快查询的，⽽B+树通过对数据进⾏排序所以是可以提⾼查询速度的，然后通过⼀个节点中可以存储多个元素，从⽽可以使得B+树的⾼度不会太⾼，在Mysql中⼀个 Innodb⻚就是⼀个B+树节点，⼀个Innodb⻚默认16kb，所以⼀般情况下⼀颗两层的B+树可以存2000万 ⾏左右的数据，然后通过利⽤B+树叶⼦节点存储了所有数据并且进⾏了排序，并且叶⼦节点之间有指针，可以很好的⽀持全表扫描，范围查找等SQL语句。

4. HashMap和ConcurrentHashMap的区别，ConcurrentHashMap的size是怎么实现的

   ConcurrentHashMap的数据结构与HashMap基本类似，区别在于：1、内部在数据 

   写入时加了同步机制(分段锁)保证线程安全，读操作是无锁操作；2、扩容时老数据的转移 

   是并发执行的，这样扩容的效率更高。

   https://www.cnblogs.com/stateis0/p/9062054.html

5. 分布式锁用过哪些，有哪些实现方式，怎么实现的，ZK、Redis

6. JVM里的垃圾收集器讲讲，CMS垃圾回收过程，CMS有什么问题，G1收集器介绍一下

7. Spring是怎么解决循环依赖的

   https://zhuanlan.zhihu.com/p/342204687

8. 谈谈对mvcc对理解，一致性非锁定读、一致性锁定读怎么实现的

   https://www.cnblogs.com/sunjingwu/p/12386660.html

9. rocket mq定时任务机制怎么实现的，谈谈时间轮算法，rocketmq事物机制是什么

   https://blog.csdn.net/QGhurt/article/details/114630705

   https://blog.csdn.net/prestigeding/article/details/121430539

   https://www.cnblogs.com/luozhiyun/p/12075326.html

   https://blog.csdn.net/qq_32099833/article/details/120247549

   https://blog.csdn.net/weixin_42405670/article/details/118313724

10. springcloud框架，降级，限流，配置中心，注册中心，链路追踪

11. spring事物传播机制

    https://blog.csdn.net/yanxin1213/article/details/100582643?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-4.pc_relevant_default&spm=1001.2101.3001.4242.3&utm_relevant_index=7

12. 数据库索引原理，锁种类及工作原理

13. 双亲委派机制，及如何打破

14. 如何优化sql，优化器原理，执行计划如何使用

15. redis为啥快

16. 日志如何处理的

17. jvm垃圾回收机制

18. java锁的种类及原理

19. 线程池参数及工作原理

20. netty的网络模型和源码实现

21. 线程池用过吗？参数怎么配置，什么情况大会使用到最大线程数

22. 线程之间共享变量有哪些方式

23. 你看过哪些中间件的源码，比如RocketMQ怎么保证保证消费成功，RocketMQ怎么提升消费和发送速度

24. 你在项目中遇到什么难点？怎么解决的

25. [给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。](https://leetcode-cn.com/problems/single-number/)

26. 数据库表数据过大怎么处理

27. 聊项目 MySQL 和 ES的数据一致性怎么做的？为什么这样做？ES如果写入失败和MySQL不一致了怎么办

28. 看你了解 RocketMQ ，了解到哪个程度了，讲讲定时任务

29. 数据库在事务查询插入的时候会有什么问题？怎么解决？如果只用数据库的时候怎么解决？

30. MySQL 的 MVCC 讲一下

31. 用线程池的时候，怎么阻塞主线程，有哪些方式？

32. SpringCloud 都用过哪些组件

33. Nacos怎么做服务注册的，流程讲一下，客户端呢

34. 谈谈你对微服务下数据一致性的看法

19. 你之前接触过高并发数据量大的场景吗

----

<center><font size=4>实习生面试题</font>
</center>

1. String 类为什么是 final 的。
2. JDK8 的 HashMap 的源码，实现原理，底层结构。
3. 反射中，Class forName 和 classloader 的区别
4. session 和 cookie 的区别和联系，session的生命周期，多个服务部著时session管理
5. Java 中的队列都有哪些，有什么区别。
6. 详谈一下 Java 的内存模型以及 GC 算法。
7. Java10、Java11 的新特性
8. Java 内存泄露的问题调查定位:jmap，jstack 的使用
9. Spring 的体系结构和 jar 用途
10. Spring MVC 的运行原理
11. Spring Boot 的执行过程
12. Spring 的事务隔离级别，实现原理
13. Spning IOC 和 AOP 的底层实现
14. Spring boot 优势和劣势，以及适用场景等
15. 讲一下 SpringCloud 和 Dubbo 的优缺点
16. 什么是 Hystrix? 它如何实现容错?
17. 什么是 Netflix Feign?它的优点是什么?
18. 谈一谈分布式一致性到 CAP 理论、BASE 理论!
19. 常用的线程池模式以及不同发程池的使用场景
20. Reentrantlock 和 synchronized 的区别
21. atomicinteger 和 volatile 等线程安全操作的关键字的理解和使用
22. 分布式锁三种实现方式
23. socket 框架 netty 的使用，以及 NIO 的实现原理，为什么是异步非阻塞的
24. 简述 NIO 的最佳实钱
25. Zookeeper 的用途，选举的原理是什么
26. 手写一个赫夫曼树

----

<center><font size=4>钱坤老师面试题</font>
</center>

1. TCP三次握手，四次挥手
2. 进程与线程区别
3. synchronized与lock区别可重入锁式如何实现的
4. HashMap结构 concurrentHashMap
5. volatile
6. JVM内存模型及其回收
7. String为什么是常量有啥好处
8. 创建线程池的几种方式，ExecutorService创建线程池可能会出现的问题
9. redis实现队列，与kafka有啥区别
10. IO编程，阻塞非阻塞
11. ArrayList与LinkList
12. spring IOC AOP及其应用
13. Spring管理的对象都是单例的，客户端并发请求服务端如何保证线程安全
14. 数据库自增主键，可能会出现什么问题，若不用自增主键作为唯一标识，有什么方案
15. wait notify
16. spring循环依赖
17. redis缓存、雪崩、击穿、穿透及解决方案
18. redis跳表
19. redis数据结构(特殊的几种及其应用)
20. mysql innodb索引类型，以及什么时候索引失效
21. mysql事务

----

京东：

>  - 1、三色标记
>  - 2、  hashmap
>  - 3、concurrentHash
>  - 4、 AQS blockqueue头结点意义：防止头位结点入、出竞争数据仓库
>  - 5、 jvm性能调优
>  - 6、 印象最深的一次

字节：

> - 1、epoll
> - 2、 redis数据类型
> - 3、mysql库有哪些，索引
> - 4、链表复制

美团：

> - 1、DDD巨细
> - 2、垃圾收集器种类
> - 3、双亲委派 打破
> - 4、数据库索引、事务隔离级别
> - 5、线程池参数
> - 6、 Object类方法有哪些
> - 7、线程运行状态、如何暂停

其他：

> - 1、kafka为啥快 和rabbitmq区别 rabbitmq几种交换机
> - 2、 aio、nio
> - 3、 cap和base理论，分布式事务哪几种、原理，seata哪几种模式
> - 4、 springboot和、springmvc和springcloud区别
> - 5、springboot如何加载初始化
> - 6、[3. 无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)



狮桥集团

1. 聊项目
2. Spring 三级缓存
3. Spring 事务的传递机制
4. RedisMySQL使用中的问题(Redis缓存不一致)
5. 讲一下自旋锁
6. 说一下 synchronized 锁升级机制
7. 说一下JVM
8. JVM 字符串常量池
9. 事务隔离级别
10. 幻读和不可重复读有什么区别
11. Netty 和 Java 的 NIO 有什么区别
12. NIO 是用什么实现的 
13. BLO 和 NIO有什么区别
14. 怎么排查一个线上问题

哗啦啦第二次面试

1. 聊项目
2. Nacos 的配置更新机制是什么？
3. 项目架构演进
4. MySQL 索引，你们用的什么事务隔离级别，为什么用可重复读，为什么用读已提交。
5. Redis怎么用的，哪些场景，缓存一致性
6. Redis 主从机制原理，流程
7. Redis 持久化机制，线上是怎么处理的
8. 分布式锁都有哪几种
9. Redission 锁的流程
10. 为什么用 RocketMQ
11. 垃圾对象的回收机制？
12. CMS垃圾收集器介绍下
13. G1垃圾收集器介绍下
14. 三色标记
15. OpenFeign 里边做了什么，了解吗
16. synchronized 偏向锁和锁消除，（锁粗化）
17. Spring 的三级缓存了解吗，怎么解决的？

奇安信
一面：

聊项目，为什么要做代码扫描？移动安全做了哪些，为什么要做这个？

Java并发包里举一个类详细介绍下

ReentrantLock 说一下详细的流程，比如临界区之类的 (没答好)

ReentrantLock 可重入的原理

说一下 CAS 

CAS 对比的过程说一下

CAS 有哪些问题

CAS 里 CompareAndSet 方法的返回值是什么

synchronized 锁升级了解吗，偏向锁怎么判断的，在哪个区域？

synchronized 的实现原理

说一下 RocketMQ 你知道的源码，举个例子，（我举个 延迟任务 ）

说一下 Redis MySQL 双写一致性

讲一下 CMS

初始标记做了什么事

并发标记做了什么事

虚拟机栈里边是什么内容？除了栈帧还有呢？

堆里边有哪些区域？比如年轻代里边

jstat、jmap、jstack 这几个平时会怎么用？

SSO 单点登录做过吗，什么原理

为什么用ES，哪些场景下用的

ES的聚合有哪些

ES 的reindex做过吗

事务的隔离级别讲一下

说一下策略设计模式

ES 的数据量多大，索引数量多少

ES 合并策略了解吗

二面：

聊项目，为什么要做代码扫描？

ES 怎么用的，用的那块

ES 的数据量多大，节点多少个

ES 状态红色碰到过吗

ES 的文档怎么设计分片的

ES 的搜索都用过哪些

你们Redis线上怎么部署的，ES线上怎么部署的

你们用Redis会做哪些事

ES 线上遇到过哪些问题？



哗啦啦
先做笔试题，不想做，大部分没做，就做了一道，还错了。

聊项目，面试官说方向不对，没有高并发经验没法问我。

开始聊项目

Spring Cloud 断路器怎么用的？断路器规则设计，不会

MQ：

rocketmq和rabbitmq的区别

RocketMQ里延迟队列怎么实现的？不会

MQ怎么保证消息一定会被消费

JVM：

cms和g1收集器的区别

怎么查找线上问题，常用的 jvm 操作命令有哪些？

并发：

用户热点数据设计（例如用户频繁修改余额）、秒杀场景，不会

ES 高并发写入和读取应该注意哪些问题。分片怎么设计，不会

有没有用过ES的统计分析？

数据库：

数据库表怎么设计，为什么要用 not null

MySQL有哪些索引

MySQL 看过哪些书？

Java：

讲讲缓存一致性

有没有用过completablefuture

用线程池应该注意哪些问题，设置队列大小，用的什么队列？
线程池满了怎么办？

AOP 用过吗，怎么用的？

平常怎么学习的？

看过哪些源码，举个例子，讲讲

用过哪些设计模式

你有哪些问我的？

[面试100问](https://www.bilibili.com/video/BV1bf4y1a7Mv?p=23)

https://blog.csdn.net/qq_35923749/category_8283721.html
